﻿using System;

namespace exercise_19
{
    class MainClass
    {
         public static void Main(string[] args)
        
        {
            Console.WriteLine($"(6 + 7) * (3 - 2) = {(6 + 7) * (3 - 2)}");
            Console.WriteLine($"(6 * 7) + (3 * 2) = {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"(6 * 7) + 3 * 2 = {(6 * 7) + 3 * 2}");
            Console.WriteLine($"(3 * 2) + 6 * 7 = {(3 * 2) + 6 * 7}");
            Console.WriteLine($"(3 * 2) + 7 * 6 / 2 = {(6 * 7) + 7 * 6 / 2}");
            Console.WriteLine($"(6 + 7 * 3 - 2 = {6 + 7 * 3 - 2}");
            Console.WriteLine($"(3 * 2 + (3 * 2) = {3 * 2 + (3 * 2)}");
            Console.WriteLine($"(6 * 7) * 7 + 6 = {(6 * 7) * 7 + 6}");
            Console.WriteLine($"(2 * 2) + 2 * 2 = {(2 * 2) + 2 * 2}");
            Console.WriteLine($"(3 * 3 + (3 * 3) = x {3 * 3 + (3 * 3)}");
            Console.WriteLine($"(6\xB2 + 7) * 3 + 2 = {(Math.Pow(6,2) + 7) * 3 + 2 }");
            Console.WriteLine($"(3 * 2) + (3\xB2 * 2) = {(3 * 2) + (Math.Pow(3,2) * 2)}");
            Console.WriteLine($"(6 * (7 + 7)) / 6 = {(6 * (7 + 7)) / 6}");
            Console.WriteLine($"((2 + 2) + (2 * 2)) = x {((2 + 2) + (2 *2))}");
            Console.WriteLine($"(4 * 4 + (3\xB2 * 3\xB2) = x {4 * 4 + (Math.Pow(3,2) * (Math.Pow(3,2)))}");

        }
    }
}